#!/bin/bash

echo "gama completa de colores que admite su terminal"
for C in {0..255}; do
    tput setab $C
    echo -n "$C "
done
tput sgr0

echo "Estándares"
echo

for C in {40..47}; do
    echo -en "\e[${C}m$C "
done

echo " 256 colores"
for C in {16..255}; do
    echo -en "\e[48;5;${C}m$C "
done
echo -e "\e(B\e[m"

echo " otros 256 colores"
for C in {16..255}; do
    echo -en "\e[38;5;${C}m$C "
done
echo -e "\e(B\e[m"