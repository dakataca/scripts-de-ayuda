#!/bin/bash

# Parámetros de entrada.
declare -a array=("$@")

# Último elemento del array, es decir el nombre del fichero.
#FILE="${array[$(($#-1))]}"
FILE="${array[-1]}"

echo "Fichero: ${FILE}"

# Elimino el último elemento del array.
unset array[-1]

# Imprimo todas las líneas.
#echo "all lines:${array[@]}:"

# Número de elementos del array actualizado.
arrayLenght=${#array[@]}
echo "arrayLenght=$arrayLenght"

for line in "${array[@]}"
do
    echo "line: ${line}"
    echo "Número de elementos del array: $((${arrayLenght}+1))"
done

:<<-!
i=0
#while (( i<=${linesLenght}  ));
while (( i<${arrayLenght}  ));
do
    echo "${array[$i]}"
#    printf '%s\n'  "line: ${array[$i]}"
    echo "Número de elementos del array: $((${arrayLenght}+1))"
    grep -o 'seat seat0 xcursor_theme Fluent-cursors 16' $HOME/.dotfiles/swaystart.sh
    ((i++))
done
!
