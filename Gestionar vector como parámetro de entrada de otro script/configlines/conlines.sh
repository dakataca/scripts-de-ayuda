#!/bin/bash

var1='línea var1'
source configlines/inslines.sh "${var1}" output

declare -a vector1=(
    'línea 1 vector1'
    'línea 2 vector1'
    'línea 3 vector1'
)
source configlines/inslines.sh "${vector1[@]}" output

declare -a vector2=(
    'línea 1 vector2'
    'línea 2 vector2'
    'línea 3 vector2'
)
source configlines/inslines.sh "${vector2[@]}" output

