#!/bin/bash

# Script que permite extraer el nombre base de un archivo con o sin 
# extensión, a partir de una ruta
# Se debe especificar la ruta (path).

path=/path/to/file.txt
echo ${s##*/}
foo.txt
s=${s##*/}
echo ${s%.txt}
foo
echo ${s%.*}
foo
