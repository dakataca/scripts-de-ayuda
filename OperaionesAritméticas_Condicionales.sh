#!/bin/bash
echo -e '============Operaciones con números en bash=====================\n'
calc '5,5 / 2'

echo 'Con bc'
echo '5.74*8' | bc -l

echo -e 'COMANDOS\n'
echo 'expr & echo: Muestran solo la parte entera del resultado'
echo -e 'calc: Incluye los decimales en el resultado'
echo -e 'let: Indica que lo que le sigue es una expresión aritmética\n'

echo '-----Usando expr (forma directa)-----'
echo "num1 = 10 num2 = 10"
echo 'Suma:';expr 10 + 10;expr $((10+10))
echo 'Resta:';expr 10 - 10;expr $((10-10))
echo 'Multiplicación:';expr 10 \* 10;expr $((10*10))
echo 'División:';expr 10 / 10;expr $((10/10))
echo 'Potencia:';expr $((10**10))
echo 'Raíz:';

echo '-----Usando expr (forma almacenada)-----'
echo "num1 = 10 num2 = 10"
echo 'Suma:';result=`expr 10 + 10`;echo $result
echo 'Resta:';result=`expr 10 - 10`;echo $result
echo 'Multiplicación:';result=`expr 10 \* 10`;echo $result
echo 'División:';result=`expr 10 / 10`;echo $result
echo 'Potencia:';result=`expr 10 ^ 10`;echo $result
echo 'Raíz:';

echo '-----Usando calc (forma directa)-----'
echo "num1 = 10 num2 = 3,5"
echo 'Suma:';calc 10+3,5;calc $((10+3,5))
echo 'Resta:';calc 10-3,5;calc $((10-3,5))
echo 'Multiplicación:';calc 10\*3,5;calc $((10*3,5))
echo 'División:';calc 10/3,5;calc $((10/3,5))
echo 'Potencia:';calc $((10^3,5))
echo 'Raíz:';

echo '-----Usando expr con variables (forma directa)-----'
num1=20
num2=5
echo "num1 = $num1 num2 = $num2"
echo 'Suma:'
expr $num1 + $num2
expr $((num1+num2))
echo 'Resta:'
expr $num1 - $num2
expr $((num1-num2))
echo 'Multiplicación:'
expr $num1 \* $num2
expr $((num1*num2))
echo 'División:'
expr $num1 / $num2
expr $((num1/num2))

echo '-----Usando expr en variable result-----'
echo "num1 = $num1 num2 = $num2"
echo 'Suma:'
result=$(expr $num1 + $num2)
echo $result
echo 'Resta:'
result=$(expr $num1 - $num2)
echo $result
echo 'Multiplicación:'
result=$(expr $num1 \* $num2)
echo $result
echo 'División:'
result=$(expr $num1 / $num2)
echo $result

echo '-----Usando echo sin variables-----'
echo 10 + 10

variable=`expr 10 / 2`
echo $variable
calc "3,5 + 5"

echo 'Condicionale sespeciales'
((50 < 50)) && echo "si" || echo "no"

echo 'Seleccionar elementos'
select option in instalar desinstalar remover copiar cagar; do
echo "$option";
case $option in
	instalar) echo Insalar seleccionado; break;;
	desinstalar) echo desinstalar seleccionado; break;;
	remover) echo removerr seleccionado; break;;
	copiar) echo copiar seleccionado; break;;
	cagar) echo cagar seleccionado; break;;
esac
done
