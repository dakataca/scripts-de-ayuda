#!/bin/bash
clear
#Dibuja una línea diagonal con los caracteres "car".

#Variables.
i=1;num=17;car="\u25BA";signo=' ';space=''
#Mientras i sea menor o igual a num.
while [ $i -le $num ]
do
	#Si i es mayor a 1 space es igual a ella misma concatenado con signo.
	if [[ $i -gt 1  ]]; then
		space+=$signo
	else
		#De lo contrario space es igual a vacio.
		space=''
	fi
#Imprimo space concatenado con caracter seguido de un espacio y tabulador.
echo -e "$space$car \t"
((i++))
done

