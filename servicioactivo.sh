#!/bin/bash

## Verifica si un servicio está o no activo.
## Omita el ".service" al especificar el servicio.

SERVICE="httpd"

if ps aux | grep -v grep | grep -v $0 |grep $SERVICE &> /dev/null ; then
	echo "Servicio $SERVICE está activo."
else
	echo "Servicio $SERVICE está inactivo."
fi
